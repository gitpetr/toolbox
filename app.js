'use strict';
let randomButton = document.querySelector('.random__button');
let cell = document.querySelectorAll('.cell');
let randomHeader = document.querySelector('.random__header');
let description = document.querySelector('.random__description');

let getRandomNumber = (min, max) => {
    let number = Math.floor(min + Math.random() * (max - min));
    return number;
  };

randomButton.addEventListener('mousedown', () => {
	let randomNumber = getRandomNumber(1, 5);
	for(let i = 0; i < cell.length; i++){
		cell[i].textContent = randomNumber;
		
		if (cell[i].textContent == 1){
			randomHeader.textContent = 'Один'
			description.textContent = 'Один'
		
		} else if (cell[i].textContent == 2){
			randomHeader.textContent = 'Два'
			description.textContent = 'Два'
		
		} else if (cell[i].textContent == 3){
			randomHeader.textContent = 'Три'
			description.textContent = 'Три'
		
		} else if (cell[i].textContent == 4){
			randomHeader.textContent = 'Четыре'
			description.textContent = 'Четыре'
		} else if (cell[i].textContent == 5){
			randomHeader.textContent = 'Пять'
			description.textContent = 'Пять'
		}
	}
});

