const game = {
  Width: 600,
  Height: 400,
  canvas: undefined,
  ctx: undefined,
  elements: [],
  mouse: { x: 0, y: 0 },
  selected: false,

  sprites: {
    and: undefined,
    nor: undefined,
    not: undefined,
    or:  undefined,
    xor: undefined
  },

  load() {
    for (var key in this.sprites) {
      this.sprites[key] = new Image();
      this.sprites[key].src = `image/${key}.png`;
    }
  },

  drawing() {
    this.ctx.clearRect(0, 0, this.Width, this.Height);
    for(el of this.elements) {
      el.draw();
      if (game.isCursorInRect(el)) {
        el.selected = true;
        el.stroke();
      } else {
        el.selected = false
      }

    }
    if(game.selected) {
      game.selected.x = this.mouse.x - game.selected.w / 2;
      game.selected.y = this.mouse.y - game.selected.h / 2;
    }

  },

  start() {
    this.load();
    this.run();
 
  },

  render() {
    this.canvas = document.getElementById("canvas")
    this.ctx = this.canvas.getContext("2d")
    this.ctx.strokeStyle = '#001EFF';
    this.ctx.lineWidth = 3;
    this.drawing();
  },

  run() {
    this.render();

    window.requestAnimationFrame(() => {
      this.run();
    })
  },

  isCursorInRect(rect) {
    return this.mouse.x > rect.x && this.mouse.x < rect.x + rect.w && this.mouse.y > rect.y && this.mouse.y < rect.y + rect.h;
  }
}

class ElementsCreator {
  constructor(el, signal) {
    this.x = 0
    this.y = 0
    this.w = 50
    this.h = 50
    this.input1 = 0
    this.input2 = 0
    this.output = 0
    this.el = el
    this.selected = false
    this.signal = signal
    game.elements.push(this)
  }

  draw() {
    game.ctx.drawImage(game.sprites[this.el], this.x, this.y, this.w, this.h);
  }

  stroke() {
    game.ctx.strokeRect(this.x, this.y, this.w, this.h)
  }
}

game.andSignal = () => {
    if (this.input1 == 1 && this.input2 == 1) {
      this.output = 1
    } else {
      this.output = 0
    }
  }

game.notSignal = () => {
  if (this.input1 == 1 && this.input2 == 1) {
    this.output = 0
  } else {
    this.output = 1
  }
}

game.norSignal = () => {
  if (this.input1 == this.input2) {
    this.output = 1
  } else {
    this.output = 0
  }
}

game.orSignal = () => {
  if (this.input1 == 0 && this.input2 == 0) {
    this.output = 0
  } else {
    this.output = 1
  }
}

game.xorSignal = () => {
  if ((this.input1 == 1 && this.input2 == 0) || (this.input1 == 0 && this.input2 == 1)) {
    this.output = 1
  } else {
    this.output = 0
  }
}

game.tooltipies = () => {
  const tooltipy = [...document.getElementsByClassName("panelButton")]
  const sprites = { ...game.sprites }
   
  for (let key in sprites) {
    tooltipy.forEach(el => {
      if (key == el['id']) {
        let container = document.createElement('p')
        container.innerHTML = game.description[[key]]
          tippy(el, {
          content: container,
          placement: "right-start",
          arrow: true,
          distance: -50,
          maxWidth: 200
        })
      }
    })
  }
}

game.description = {
  and: `Элемент AND
        .now() bbbbbbbbbbbbbbbbb
        bbbbbbbbbbbbbbbbb
        bbbbbbbbbbbbbbbbbbb
        bbbbbbbbbbbbbbbbbb
        b
        b`,
  nor: `NOR 
        В Кабо-Верде местная полиция прихватила российский 
        корабль (понятно, под панамским флагом) и 11 российскими
         моряками с грузом в 9,5 тонн кокаина. 
         Очень возможно, что целью торгово-обменной операции,
          как и в случае с золотым запасом, является снабжение
           братского мафиозного режима Мадуро-Кабельо порцией 
           наличности для бесперебойного функционирования
         структур управления венесуэльским картелем.`,
  not: `NOT`,
  or: 'OR',
  xor: 'XOR'
}

game.deleteElement = () => {
  const idx = game.elements.findIndex((el) => el.selected );
  game.elements.splice(idx, 1);
}


window.addEventListener("load", () => {
  game.start();
  document.getElementById('and').addEventListener('click', () => {
    new ElementsCreator('and', game.andSignal);
  })

  document.getElementById('nor').addEventListener('click', () => {
    new ElementsCreator('nor', game.norSignal);
  })

  document.getElementById('or').addEventListener('click', () => {
    new ElementsCreator('or', game.orSignal);
  })

  document.getElementById('not').addEventListener('click', () => {
    new ElementsCreator('not', game.notSignal);
  })

  document.getElementById('xor').addEventListener('click', () => {
    new ElementsCreator('xor', game.xorSignal);
  })
 
  game.canvas.addEventListener('mousemove', e => {
    game.mouse.x = e.layerX;
    game.mouse.y = e.layerY;
  })

  window.addEventListener('mousedown', () => {
    if(!game.selected) {
      for (el of game.elements) {
        if (game.isCursorInRect(el)) {
          game.selected = el;
        }
      }
    }
  })

  window.addEventListener('mouseup', () => game.selected = false)

  window.addEventListener("keydown", event => {
    if (event.keyCode == 46) {
      game.deleteElement();
    }
  })

  game.tooltipies();
})
